// Coding Challenge 128: SketchRNN Snowflakes
// Daniel Shiffman
// https://thecodingtrain.com/CodingChallenges/128-sketchrnn-snowflakes.html
// https://youtu.be/pdaNttb7Mr8
// https://editor.p5js.org/codingtrain/sketches/B1NHS00xE

let model, nextStroke, currentPoint, penState;

function gotSketch(error, stroke) {
  if (error) {
    console.error(error);
  } else {
    nextStroke = stroke;
  }
}

function resetPosition() {
  currentPoint = [random(-width / 2, width / 2), random(-height / 2, height / 2)];
  penState = "down";
  nextStroke = null;
}

function resetModel() {
  model.reset();
  model.generate(gotSketch);
}

function setup() {
  createCanvas(windowWidth, windowHeight);
  resetPosition();
  model = ml5.SketchRNN("snowflake", resetModel);
  background(0);
}

function draw() {
  translate(width / 2, height / 2);
  if (nextStroke !== null) {
    let nextPoint = [currentPoint[0] + nextStroke.dx * 0.2, currentPoint[1] + nextStroke.dy * 0.2];
    if (penState === "down") {
      stroke(255);
      strokeWeight(2);
      line(currentPoint[0], currentPoint[1], nextPoint[0], nextPoint[1]);
    }
    penState = nextStroke.pen;
    currentPoint = [nextPoint[0], nextPoint[1]];
    if (penState !== "end") {
      model.generate(gotSketch);
    } else {
      resetModel();
      resetPosition();
    }
  }
}